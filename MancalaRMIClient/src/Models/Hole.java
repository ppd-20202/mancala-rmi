/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import javax.swing.JLabel;

/**
 *
 * @author thiag
 */
public class Hole implements Remote{
    
    private int id;
    private int stones;
    private JLabel label;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStones() {
        return stones;
    }

    public void setStones(int stones) {
        this.stones = stones;
    }

    public JLabel getLabel() {
        return label;
    }

    public void setLabel(JLabel label) {
        this.label = label;
    }

   
    public Hole(int id, int stones, JLabel label){
        this.id = id;
        this.stones = stones;
        this.label = label;
    }
    
    public Hole() throws RemoteException{
    }
    
}

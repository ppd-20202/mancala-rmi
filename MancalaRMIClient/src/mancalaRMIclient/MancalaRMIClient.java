/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mancalaRMIclient;


import Interfaces.Player;
import View.TelaPrincipal;
import java.rmi.Naming;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.PLAIN_MESSAGE;
import static javax.swing.JOptionPane.showInputDialog;
import static javax.swing.JOptionPane.showMessageDialog;
import Interfaces.ServerItf;
import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;



/**
 *
 * @author thiag
 */
public class MancalaRMIClient extends UnicastRemoteObject implements Player{

     /**
     * @throws java.rmi.RemoteException
     */
    
    public TelaPrincipal tela;

    public void setTela(TelaPrincipal tela) {
        this.tela = tela;
    }
    
    public MancalaRMIClient() throws RemoteException{
        super();
    }
    
    public static void main(String[] args) throws RemoteException, NotBoundException, MalformedURLException, AlreadyBoundException {

        String playerName = "";

        String address = "127.0.0.1";
        int port = 5000;
        boolean validationConnection = true;
        TelaPrincipal tela;
        MancalaRMIClient client = null;
        boolean registrySuccess = false;
        boolean verifyPlayerName = false;
        ServerItf server = null;
     
        do {
            try {
                address = showInputDialog(null, "Digite o endereço do servidor", "", PLAIN_MESSAGE);
                port = Integer.parseInt(showInputDialog(null, "Digite a porta de acesso", "", PLAIN_MESSAGE));
            } catch (NumberFormatException e) {
                showMessageDialog(null, "Digite um endereço e um número de porta válidos", "", ERROR_MESSAGE);
                validationConnection = true;
            }
        } while (!validationConnection);
        
            
        do {
            playerName = showInputDialog(null, "Digite seu nome", "", PLAIN_MESSAGE);
            
            try{
                String serverURL = "rmi://" + address + ":" + port + "/RMIServer";
                server = (ServerItf) Naming.lookup(serverURL);
                
                verifyPlayerName = server.verifyPlayerName(playerName) || !playerName.isEmpty();
                
                if(!verifyPlayerName){
                    if(playerName.isEmpty()){
                        showMessageDialog(null, "Nomes vazios não são aceitos.", "", ERROR_MESSAGE);
                    }else{
                        showMessageDialog(null, "Nome já em uso. Por favor digite outro.", "", ERROR_MESSAGE);
                    }
                }else{
                    client = new MancalaRMIClient();
                    String clientUrl = "//" + address + ":" + port + "/" + playerName;
                    Naming.bind(clientUrl, client);
              
                    registrySuccess = server.registerPlayer(playerName);
                    
                    if(!registrySuccess){
                        showMessageDialog(null, "Os dois jogadores já estão conectados.", "", ERROR_MESSAGE);
                        System.exit(0);
                    }
                }
                
            }catch (RemoteException ex) {
                showMessageDialog(null, "Erro ao estabelecer conexão.", "", ERROR_MESSAGE);
                System.err.println(ex.getMessage());
                ex.printStackTrace(System.err);
                registrySuccess = false;
            }
        } while (!verifyPlayerName || !registrySuccess);

        tela = new TelaPrincipal(playerName, server);
        client.setTela(tela);
        boolean isMyTurn = server.iBeginTheGame(playerName);
        
        
        if(!isMyTurn){
            tela.sendNameToOpponent();
            tela.opponentName = server.getOpponentName(playerName);
            tela.setOpponentName();
            tela.passTurn();
        }
        tela.setVisible(true);
        tela.setSize(725, 525);
        tela.setResizable(false);
    }
    
    @Override
    public void opponentWantReset() throws RemoteException {
        tela.opponentWantReset();
    }
    
    @Override
    public void opponentResponseAboutReset(boolean response) throws RemoteException{
        tela.opponentResponseAboutReset(response);
    }
    
    @Override
    public void opponentGiveUp() throws RemoteException{
        tela.opponentGiveUp();
    }
    
    @Override
    public void beginTurn(ArrayList<String> holes) throws RemoteException{
        tela.beginTurn(holes);
    }
    
    @Override
    public void endOfGame(String result) throws RemoteException{
        tela.endOfGame(result);
    }
    
    @Override
    public void receiveNewMessage(String message) throws RemoteException{
        tela.receiveNewMessage(message);
    }

    @Override
    public void opponentName(String opponentName) throws RemoteException {
        tela.opponentName(opponentName);
    }
    
}

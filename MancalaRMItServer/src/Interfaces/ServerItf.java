/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 *
 * @author isaaccavalcante
 */
public interface ServerItf extends Remote{
    boolean registerPlayer(String playerName) throws RemoteException;
    boolean verifyPlayerName(String playerName) throws RemoteException;
    boolean iBeginTheGame(String playerName) throws RemoteException;
    String getOpponentName(String playerName) throws RemoteException;
    void sendNameToOpponent(String playerName) throws RemoteException;
    void sendMesage(String message, String playerName) throws RemoteException;
    void opponentWantReset(String playerName) throws RemoteException;
    void opponentResponseAboutReset(boolean response, String playerName) throws RemoteException;
    void opponentGiveUp(String playerName) throws RemoteException;
    void passTurn(ArrayList<String> holes, String playerName) throws RemoteException;
    void endOfGame(String result, String playerName) throws RemoteException;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 *
 * @author isaaccavalcante
 */
public interface Player extends Remote{
    void opponentName(String opponentName) throws RemoteException;
    void opponentWantReset() throws RemoteException;
    void opponentResponseAboutReset(boolean response) throws RemoteException;
    void opponentGiveUp() throws RemoteException;
    void beginTurn(ArrayList<String> holes) throws RemoteException;
    void endOfGame(String result) throws RemoteException;
    void receiveNewMessage(String message) throws RemoteException;
}

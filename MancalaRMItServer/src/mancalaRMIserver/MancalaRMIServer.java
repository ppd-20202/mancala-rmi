/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mancalaRMIserver;
import Interfaces.Player;
import java.rmi.RemoteException;
import Interfaces.ServerItf;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author isaaccavalcante
 */
public class MancalaRMIServer extends UnicastRemoteObject implements ServerItf{
    String player1Name;
    String player2Name;
    Player player1 = null;
    Player player2 = null;
    int port;
    /**
     * @throws java.rmi.RemoteException
     */
    
    protected MancalaRMIServer() throws RemoteException{
    }

    public MancalaRMIServer(int port) throws RemoteException {
        super();
        this.port = port;
        System.out.println("Servidor criado!");
    }
    
    private Player getPlayer(String playerName){
        if(playerName.equals(player1Name)){
            return player2;
        }else if(playerName.equals(player2Name)){
            return player1;
        }else{
            return null;
        }
    }

    @Override
    public boolean registerPlayer(String playerName) throws RemoteException {
        String clientUrl = "rmi://127.0.0.1:" + port + "/" + playerName;
        if(player1 == null){
            player1Name = playerName;
            try {
                player1 = (Player) Naming.lookup(clientUrl);
                System.out.println("REGISTROU PLAYER 1");
                System.out.println(playerName);
            } catch (NotBoundException | MalformedURLException ex) {
                Logger.getLogger(MancalaRMIServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if(player2 == null){
            player2Name = playerName;
            try {
                player2 = (Player) Naming.lookup(clientUrl);
                System.out.println("REGISTROU PLAYER 2");
                System.out.println(playerName);
            } catch (NotBoundException | MalformedURLException ex) {
                Logger.getLogger(MancalaRMIServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else{
            return false;
        }
        return true;
    }
    
    @Override
    public boolean verifyPlayerName(String playerName){
        if(player1Name != null){
            if(player1Name.equals(playerName)) return false;
        }else{
            return true;
        }
        return true;
    }

    @Override
    public void sendMesage(String message, String playerName) throws RemoteException {
        Player player = getPlayer(playerName);
        if(player != null) player.receiveNewMessage(message);
    }

    @Override
    public void opponentWantReset(String playerName) throws RemoteException {
        Player player = getPlayer(playerName);
        if(player != null) player.opponentWantReset();
    }
    
    @Override
    public void opponentResponseAboutReset(boolean response, String playerName) throws RemoteException {
        Player player = getPlayer(playerName);
        if(player != null) player.opponentResponseAboutReset(response);
    }

    @Override
    public void opponentGiveUp(String playerName) throws RemoteException {
        Player player = getPlayer(playerName);
        if(player != null) player.opponentGiveUp();
    }

    @Override
    public void endOfGame(String result, String playerName) throws RemoteException {
        Player player = getPlayer(playerName);
        if(player != null) player.endOfGame(result);
    }

    @Override
    public void sendNameToOpponent(String playerName) throws RemoteException {
        Player player = getPlayer(playerName);
        if(player != null) player.opponentName(playerName);
    }

    @Override
    public void passTurn(ArrayList<String> holes, String playerName) throws RemoteException {
        Player player = getPlayer(playerName);
        if(player != null) player.beginTurn(holes);
    }

    @Override
    public boolean iBeginTheGame(String playerName) throws RemoteException {
        return (playerName == null ? player1Name == null : playerName.equals(player1Name));
    }

    @Override
    public String getOpponentName(String playerName) throws RemoteException {
        if(playerName.equals(player1Name)){
            return player2Name;
        }else{
            return player1Name;
        }
    }
    
}
